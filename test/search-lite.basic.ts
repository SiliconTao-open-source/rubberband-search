import { SearchLite } from '../src/search-lite';

// import assert from 'assert';
const assert = require('assert');

let sqlite = require('sqlite3').verbose();
let Sql;
let DbFile = 'test_database.db';
let searchLite: SearchLite;
let getCalled = 0;

describe('Testing simple searching', () => {

	beforeEach( () => {
        return new Promise( (resolve, reject) => {
            Sql = new sqlite.Database(DbFile, (err: any) => {
                searchLite = new SearchLite(Sql, false);

                let insertCmd: string;
                let argsSupplied = new Array<string>;

                Sql.run = (cmdStr: string, argStrings: string[], errorBack: any) => {
                    if(!insertCmd) { insertCmd = cmdStr; }
                    argsSupplied = argStrings;
                }

                getCalled = 0;
                Sql.get = ( (cmdStr: string, callbacks: { errorBack: any, respondBack: any }) => {

                    getCalled++;
                    callbacks.respondBack( { 'id': 45 } );
                });

                resolve('DB ready');
            });
        })
    });
	after( () => {});

    it("should return 32", async () => {
        searchLite.setInternalFrequency(32);
        assert.equal(searchLite.setInternalFrequency() , 32);
    });
    it("should return 19", async () => {
        // @ts-expect-error
        searchLite.createTag('bubbles', () => {
            assert.equal(getCalled, 5, 'Number of get calls');
        });
    });
    it('should make 6 tags', async () => {
        searchLite.delimiterSpanners('-');
        let countTags = 0;

        // @ts-expect-error
        searchLite.createTag = ( (t, callback: any) => {
            console.log('creatTag %s', t);
            countTags++;
        });

        searchLite.processByteStream('sample', 1, 'CFM-109', 'testing');
        assert.equal(countTags, 6, 'Number of tags in CFM-109');
    });
    it('should make 6 tags', async () => {
        searchLite.delimiterSpanners('.');
        let countTags = 0;

        // @ts-expect-error
        searchLite.createTag = ( (t, callback: any) => {
            console.log('creatTag %s', t);
            countTags++;
        });

        searchLite.processByteStream('sample', 1, 'CFM.109', 'testing');
        assert.equal(countTags, 6, 'Number of tags in CFM-109');
    });
    it('should make 2 tags', async () => {
        searchLite.delimiterSpanners('_');
        let countTags = 0;

        // @ts-expect-error
        searchLite.createTag = ( (t, callback: any) => {
            console.log('creatTag %s', t);
            countTags++;
        });

        // This hyphen is not part of the delimiter spanners there for it will break the word
        searchLite.processByteStream('sample', 1, 'CFM-109', 'testing');
        assert.equal(countTags, 2, 'Number of tags in CFM-109');
    });
    it('should make 38 tags', async () => {
        searchLite.delimiterSpanners('-');
        let countTags = 0;

        // @ts-expect-error
        searchLite.createTag = ( (t, callback: any) => {
            //console.log('creatTag %s', t);
            countTags++;
        });

        searchLite.processByteStream('sample', 1, 'ABCD-1234-EFGH-5678', 'testing');
        assert.equal(countTags, 38, 'Number of tags in ABCD-1234-EFGH-5678');
    });
});
