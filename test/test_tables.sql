CREATE TABLE IF NOT EXISTS 'users' (
   'id' INTEGER PRIMARY KEY,
   'firstname' VARCHAR(50) NOT NULL,
   'lastname' VARCHAR(50) NOT NULL,
   'username' VARCHAR(50) NOT NULL UNIQUE,
   'password' VARCHAR(50) NOT NULL,
   'emailAddress' VARCHAR(50) NOT NULL,
   'twitterAccount' VARCHAR(50),
   'changePassRequired' BOOLEAN DEFAULT FALSE);

CREATE TABLE IF NOT EXISTS 'lorem_ipsum' (
   'id' INTEGER PRIMARY KEY,
   'lorem_ipsum' VARCHAR(500) NOT NULL);
