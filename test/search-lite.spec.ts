import { error } from 'console';
import { SearchLite } from '../src/search-lite';
import { buffer } from 'stream/consumers';
import { resolve } from 'path';
import { rejects } from 'assert';

let searchLite: SearchLite;
const assert = require('assert');
let fs = require('fs');
let sqlite = require('sqlite3').verbose();
let Sql;
let DbFile = 'test_database.db';

function DbSetup(DbFile: string, callback: any, errorBack: any) {
	DeleteTempFiles();
	Sql = new sqlite.Database(DbFile, (err: any) => {
		if (err) {
			console.error('Could not connect to database', err);
			errorBack('Could not connect to database');
		} else {
			console.info('Connected to database');
			searchLite = new SearchLite(Sql, false, 500);
			// Does SQL have any execution stats? qlite.org/optoverview.html
			// Create some tables then assign some scans to the tables
			let fileName = 'test/test_tables.sql';
			fs.readFile(fileName, 'utf8', (error, buffer) =>{
				if(error) {
					console.error('Error loading test SQL: %s', error.message);
					errorBack(error.message);
				} else {
					console.info('Building %s', DbFile);
					Sql.exec('' + buffer, (err) => {
						if(err) {
							console.error('Error in %s: %s', fileName, err.message);
							errorBack(err.message);
						} else {
							callback('Test DB ready');
						}
					})
				}
			})
		}
	});
}

function InsertData(newText: string, callback: any) {
	Sql.run('INSERT INTO lorem_ipsum (lorem_ipsum) VALUES (?)', newText, (error) => {
		if(error) {
			console.error('Insert error %s', error.message);
		} else {
			callback();
		}
	});
}

function DeleteTempFiles() {
	if(fs.existsSync(DbFile)) {
		fs.unlinkSync(DbFile, () => {});
	}

}

function WatchForTrigger(callback: any) {
	Sql.all('SELECT * FROM sqlite_master WHERE type = "trigger"', (error, results) => {
		if(error) {
			console.error('Error %s', error.message);
		} else {
			let redo = true;
			if(results.find( (t) => t['name'] === 'scan_after_update_lorem_ipsum')) {
				callback();
			} else {
				setTimeout( () => {
					WatchForTrigger(callback);
				}, 50);
			}
		}
	});
}

function SearchForString(searchString: string, callback: any) {
	searchLite.search([searchString], (results) => {
		callback(Object.keys(results).length);
	}, 'test');
}

function WaitForTags(callback: any) {
	setTimeout( () => {
		Sql.get('SELECT COUNT(*) AS COUNT FROM searchLiteTags', (error, result) => {
			if(error) {
				console.error(error.message);
			} else {
				if(result['COUNT'] > 0) {
					callback();
				} else {
					WaitForTags(callback);
				}
			}
		});
	}, 50);
}

function WatchEvents(callback: any, errorBack: any) {
	setTimeout( () => {
		Sql.get('SELECT COUNT(*) AS COUNT FROM searchLiteEvents', (error, result) => {
			if(error) {
				errorBack(error.message);
			} else {
				if(result['COUNT'] === 0) {
					callback();
				} else {
					WatchEvents(callback, errorBack);
				}
			}
		});
	}, 50);
}

describe('Testing simple searching', () => {

	before( () => {
		return new Promise( (resolve, reject) => {
			DbSetup(DbFile, (resultMessage: string) => {
				console.info('%s', resultMessage);
				searchLite.registerEvent('lorem_ipsum', [ 'lorem_ipsum' ], 'test');
				WatchForTrigger( () => {
					fs.readFile('test/lorem_ipsum.txt', 'utf8', (error, buffer) => {
						if(error) {
							reject('Error reading source file ' + error.message);
						}

						buffer.split('\n').forEach( (l, i, a) => {
							InsertData(l, () => {
								if (Object.is(a.length - 1, i)) {
									WaitForTags( () => {
										WatchEvents(() => {
											console.info('Test DB populated');
											resolve('DB populated');
										}, (errorMessage: string) => {
											reject(errorMessage);
										});
									})
									searchLite.runAllEvents();
								}
							});
						});
					});
				});
			}, (errorMessage: string) => {
				console.error(errorMessage);
			});
		});
	});
	after( () => {
		searchLite.stats( (stats) => {
			if(stats) {
				Object.entries(stats).forEach( (e) => {
					let [k, v] = e;
					console.info('%s: %s', k, v);
				});
			}
			setTimeout( () => {
				DeleteTempFiles();
			}, 10);
		})
	});
	it('Should find 9 matches of "dign"', () => {
		return SearchForString('dign', (result) => {
			assert.equal(result, 9, 'Returned a message thing');
		});
    });
	it('Should find 15 matches of "acc"', () => {
		return SearchForString('acc', (result) => {
			assert.equal(result, 15, 'Returned a message thing');
		});
    });
	it('Should find 14 matches of "recus"', () => {
		return SearchForString('recus', (result) => {
			assert.equal(result, 14, 'Returned a message thing');
		});
	});

});
