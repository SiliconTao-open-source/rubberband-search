CREATE TABLE IF NOT EXISTS 'searchLiteEvents' (
    'id' INTEGER PRIMARY KEY,
    'tableName' VARCHAR(200),
    'tupleId' INT,
    UNIQUE ('tableName', 'tupleId'));

CREATE TABLE IF NOT EXISTS 'searchLiteTags' (
    'id' INTEGER PRIMARY KEY,
    'tag' VARCHAR(50));

CREATE TABLE IF NOT EXISTS 'searchLiteTables' (
	'id' INTEGER PRIMARY KEY,
	'tableName' VARCHAR(50));

CREATE TABLE IF NOT EXISTS 'searchLiteCategories' (
	'id' INTEGER PRIMARY KEY,
	'category' VARCHAR(50));

CREATE TABLE IF NOT EXISTS 'searchLiteLocators' (
    'id' INTEGER PRIMARY KEY,
    'tagId' INTEGER,
    'tableId' INTEGER,
    'tupleId' INTEGER,
    'categoryId' INTEGER);

CREATE TRIGGER scan_after_insert_users 
   AFTER INSERT ON users
BEGIN
   INSERT INTO searchLiteEvents
   (tableName, tupleId) VALUES ('users', new.id);
END;

CREATE TRIGGER scan_after_update_users 
   AFTER UPDATE ON users
BEGIN
   INSERT INTO searchLiteEvents
   (tableName, tupleId) VALUES ('users', new.id);
END;