-- Categories are optional but can refine results quickly
-- When searching with a category text matches from other categories
-- or no category will not be included in the result.
CREATE TABLE IF NOT EXISTS 'searchLiteCategories' (
	'id' INTEGER PRIMARY KEY,
    'triggerId' INTEGER,
	'category' VARCHAR(50));

