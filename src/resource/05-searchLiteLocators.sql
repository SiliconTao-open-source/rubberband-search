-- The large table of seaching indexed data.
-- Searches get results from this table using only integer values.
CREATE TABLE IF NOT EXISTS 'searchLiteLocators' (
    'tagId' INTEGER,
    'tableId' INTEGER,
    'tupleId' INTEGER,
    'categoryId' INTEGER,
    PRIMARY KEY ('tagId', 'tableId', 'tupleId', 'categoryId') );

