-- When a table is updated a trigger will add a unique entry to this 
-- table so the scanner will be able to process the data in the record.
CREATE TABLE IF NOT EXISTS 'searchLiteEvents' (
    'id' INTEGER PRIMARY KEY,
    'tableId' INTEGER,
    'tupleId' INTEGER,
    UNIQUE ('tableId', 'tupleId'));
