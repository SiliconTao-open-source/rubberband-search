-- Normalize the table names to integers for faster searching
CREATE TABLE IF NOT EXISTS 'searchLiteTables' (
	'id' INTEGER PRIMARY KEY,
	'tableName' VARCHAR(50),
    UNIQUE( 'tableName' ));
