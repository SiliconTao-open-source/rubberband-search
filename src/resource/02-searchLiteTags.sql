-- Table for searchable tags. The first three to five letters of any word.
-- Search values of six or more can be handled in the browser to off load the work.
CREATE TABLE IF NOT EXISTS 'searchLiteTags' (
    'id' INTEGER PRIMARY KEY,
    'tag' VARCHAR(50),
    UNIQUE('tag'));

