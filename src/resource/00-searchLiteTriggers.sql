-- When a trigger is registered it is also adding to the triggers
-- table to identifyt the column needing to be scaned.
CREATE TABLE IF NOT EXISTS 'searchLiteTriggers' (
    'id' INTEGER PRIMARY KEY,
    'tableId' INTEGER,
    'column' VARCHAR(50),
    UNIQUE ('tableId', 'column'));
