import * as sqlite from 'sqlite3';
import fs from 'fs';

export type RegistrationStackT = {
    tableName: string;
    columnNames: string[];
    category: string;
    callback?: any;
}

export class SearchLite {
    private internalTimer: number = 60000;
    private db: sqlite.Database;
    private createFinished: boolean = false;
    private regStackPending: RegistrationStackT[] = [];
    private regStack: RegistrationStackT[] = [];
    private tablesWithTriggers: string[] = [];
    private activeScanLimit: number = 1;
    private currentScanEvents: Array<number>;
    private patterMatcher: string;
    private delimiterSpannersV!: string;

    constructor(db: sqlite.Database, useInternalEventTrigger: boolean, activeScanLimit?: number,
        delimiterSpannersV?: string) {
        this.db = db;
        if(activeScanLimit) {
            this.activeScanLimit = activeScanLimit;
        }
        this.currentScanEvents = new Array();
        this.patterMatcher = '[a-zA-Z0-9]';
        if(delimiterSpannersV) {
            if(delimiterSpannersV.match(/ /)) {
                console.error('SearchLite error: spaces not allowed in delimiter spanners. Spaces removed.');
                delimiterSpannersV = delimiterSpannersV.replace(/ /g, '');
            }
            this.delimiterSpannersV = delimiterSpannersV;
            this.patterMatcher = '[a-zA-Z0-9' + this.delimiterSpannersV + ']';
        }

        const path = require('path').resolve(__dirname, '..');
        fs.readdir(path + '/src/resource/', (err: any, files: any ) => {
            if (err) {
                console.error('Unable to scan directory: ' + err);
            } else {
                const orgFileLength = files.length;
                let completeFilesCount = 0;
                files.sort().forEach( (file: string) => {

                    fs.readFile(path + '/src/resource/' + file, 'utf8', (fsError, sqlBuffer) => {
                        if(fsError) {
                            console.error('search-lite:readFile error %s', fsError);
                        } else {
                            db.run(sqlBuffer, (sqlError) => {
                                if(sqlError) {
                                    console.error('search-lite:sqlError %s', sqlError);
                                }
                                completeFilesCount++;
                                if(completeFilesCount === orgFileLength) {
                                    this.createFinished = true;
                                    this.processRegistrationStack();
                                }
                            })
                        }
                    });
                })
            }
        });

        if(useInternalEventTrigger) {
            this.setInternalTimer()
        }

        // 5-unique-violation-can-happen-if-trigger-hits-table-before-event-cleared
        // triggers can have multiple columns within one trigger, this can be called many time
        this.db.all('SELECT name, sql FROM sqlite_master WHERE type = "trigger" AND sql LIKE "% INTO searchLiteEvents %"',
            (aError, aResults: Array<{ name: string, sql: string }>) => {
            if(aError) {
                console.error('Error trying to find old triggers. %s', aError.message);
            } else {
                aResults.forEach( (t) => {
                    this.db.run('DROP TRIGGER IF EXISTS ' + t?.['name'], (dError) => {
                        if(dError) {
                            console.error('Could not drop old trigger. %s', dError.message);
                        } else {
                            this.db.run(t.sql.replace('BEGIN INSERT INTO searchLiteEvents',
                                'BEGIN REPLACE INTO searchLiteEvents'), (cError) => {
                                if(cError) {
                                    console.error('Error creating trigger. %s', cError.message);
                                }
                            })
                        }
                    });
                })
            }
        })
    }

    /**
     * Set or Get the delimiter spanners value
     * @param newDelimiterSpanners
     * @returns
     */
    public delimiterSpanners(newDelimiterSpanners?: string): string {
        if(newDelimiterSpanners) {
            this.delimiterSpannersV = newDelimiterSpanners;
            this.patterMatcher = '[a-zA-Z0-9' + this.delimiterSpannersV + ']';
        }
        return this.delimiterSpannersV;
    }

    private setInternalTimer() {
        if(this.createFinished) {
            if(this.regStackPending.length > 0) {
                this.processRegistrationStack();
            }
            this.runEvents();
        }
        setTimeout( () => this.setInternalTimer(), this.internalTimer);
    }

    public setInternalFrequency(newMilliSeconds?: number): number {
        if(newMilliSeconds) {
            this.internalTimer = newMilliSeconds;
            this.setInternalTimer();
        }
        return this.internalTimer;
    }

    private removeEvent(eventId: number) {
        this.db.run('DELETE FROM searchLiteEvents WHERE id = ?', [eventId], (error) => {
            if(error) {
                console.error('removeEvent(%s): %s', eventId, error);
            }
            // Remove an item from an array
            this.currentScanEvents = this.currentScanEvents.filter( (i) => i !== eventId);
        })
    }

    public runAllEvents() {
        this.db.get('SELECT COUNT(*) AS COUNT FROM searchLiteEvents', (error, result: any) => {
            if(error) {
                console.error('runAllEvents error %s', error.message);
            } else {
                console.info('search-lite: running all %d events', result['COUNT']);
                if(result['COUNT'] > 0) {
                    this.runEvents();
                    setTimeout( () => this.runAllEvents(), 5);
                }
            }
        })
    }

    // external event entry point
    public runEvents() {
        // Running multiple scans at the same time causes duplication as a record is still being processes
        // others attempt to do the same work.
        if(this.currentScanEvents.length < this.activeScanLimit) {
            // get the content of the events table, get {tableName, tupleId}
            this.db.all('SELECT rE.id, rE.tableId, rE.tupleId, rTb.tableName, rTg.column, rC.category FROM searchLiteEvents rE JOIN searchLiteTables rTb ON rE.tableId = rTb.id ' +
                    'JOIN searchLiteTriggers rTg ON rTg.tableId = rTb.id JOIN searchLiteCategories rC ON rC.triggerId = rTg.id WHERE rE.tupleId > 0 LIMIT ?', this.activeScanLimit,
                    (error: any, result: { id: number, tableId: number, tupleId: number, tableName: string, column: string, category: string }[]) => {
                if(error) {
                    console.error('searchLite:runEvents 166 %s', error);
                } else {
                    if(result.length > 0) {
                        console.info('search-lite: running %d events', result.length);
                    }
                    result.forEach( (r: any) => {
                        if(this.currentScanEvents.find( (i) => i === r.id)) {
                            return;
                        }
                        this.currentScanEvents.push(r.id);
                        this.removeEvent(r['id']);

                        this.regStack.forEach( (rS) => {
                            if(rS.tableName == r['tableName']) {
                                if(rS.columnNames.length > 0) {
                                    this.db.get('SELECT ' + rS.columnNames + ' FROM ' + rS.tableName + ' WHERE id = ?', r['tupleId'],
                                            (bError, bResult: any) => {
                                        if(bError) {
                                            console.error('searchLite:runEvents 186 %s', bError);
                                        } else {
                                            let byteStream = Object.keys(bResult).map((k) => { return bResult?.[k] }).join();
                                            this.processByteStream(r['tableName'], r['tupleId'], byteStream, rS.category);
                                        }
                                    });
                                }
                                if(rS.callback) {
                                    rS.callback(r['tupleId'], (byteStream: string) => {
                                        this.processByteStream(r['tableName'], r['tupleId'], byteStream, rS.category);
                                    })
                                }

                            }
                        })
                    });
                }
            });
        }
    }

    private optimizeByteStream(byteStream: string): string {
        //console.warn('Improve performance by sorting words and removing duplicate: byteStream.length %s', byteStream.length);
        // should this use the pattern variable here
        byteStream = byteStream?.replace(/[^a-z0-9._-]/gi, ' ');
        let byteArray = byteStream?.split(" ");
        byteArray = byteArray?.filter( (s) => s.length > 2);
        let uniqueByteArray = [...new Set(byteArray)];
        byteStream = uniqueByteArray?.toString();
        //console.warn('Improve performance further by removing common grammar: byteStream.length %s', byteStream.length);
        return byteStream;
    }

    public processByteStream(tableName: string, tupleId: number, byteStream: string, category: string) {
        byteStream = this.optimizeByteStream(byteStream);
        if(byteStream) {

            // parse the byteStream, skip non letters, eat letters until non letter, send letter block to tagCreator
            let tagStack = new Array<string>;
            tagStack.push('');
            for (let i = 0; i < byteStream.length; i++) {
                let letter = byteStream.charAt(i);

                if(letter.match(this.patterMatcher)) {
                    tagStack.forEach( (t, i) => {
                        tagStack[i] = t + letter;

                        if(tagStack[i].length > 2) {
                            let workingTag = tagStack[i];
                            this.createTag(workingTag, (tagId: number) => {
                                this.addToLocators(tagId, tableName, tupleId, category);
                            })
                        }
                    });
                    if((this.delimiterSpannersV) && (this.delimiterSpannersV.match(letter))) {
                        tagStack.push('');
                        // start a new tag, add it to tag stack
                    }
                } else {
                    tagStack = [];
                    tagStack.push('');
                }
            }
        }
    }

    private addToLocators(tagId: number, tableName: string, tupleId: number, category: string) {
        let categoryField = '';
        let categoryMark = '';
        let queryArray = [ tagId, tableName, tupleId ];
        if(category) {
            categoryMark = ', (SELECT id FROM searchLiteCategories WHERE category = ?)';
            categoryField = ', categoryId';
            queryArray.push(category);
        }

        // let QueryString = 'REPLACE INTO searchLiteLocators (tagId, tableId, tupleId' + categoryField + 
        //     ') VALUES (%s, (SELECT id FROM searchLiteTables WHERE tableName = %s), %s' + 
        //     categoryMark.replace('?', '%s') + ')';
        //console.log(QueryString.toString(), queryArray[0], queryArray[1], queryArray[2], queryArray[3], queryArray[4]);
        //console.log('queryArray %s', queryArray);
        this.db.run('REPLACE INTO searchLiteLocators (tagId, tableId, tupleId' + categoryField + ') VALUES (' +
            '?, (SELECT id FROM searchLiteTables WHERE tableName = ?), ?' + categoryMark + ')', queryArray,
            (error: string) => {
                if(error) {
                    console.error('addToLocators error %s', error);
                }
            });
    }

    private createTag(newTag: string, callBack: any) {
        // replace if not exist for each sub text of three or more letters
        let lcNewTag = newTag.toLowerCase();
        this.db.run('INSERT OR IGNORE INTO searchLiteTags (tag) VALUES (?)', [ lcNewTag ], (err) => {
            if(err) {
                console.error('searchLite:createTag %s', err);
            } else {
                this.db.get('SELECT id FROM searchLiteTags WHERE tag = ?', [lcNewTag], (e, r: any) => {
                    if(e) {
                        console.error('searchLite:createTag %s', e);
                    } else {
                        callBack(r['id']);
                    }
                });
            }
        })
    }

    // Scan records for each record of each table listed in searchLiteTriggers
    public scanAllData() {
        if(this.createFinished) {
            this.db.run('DELETE FROM searchLiteTags' , (tagsError) => {
                if(tagsError) {
                    console.warn('Error deleting records from tags %s', tagsError);
                }
                this.db.run('DELETE FROM searchLiteLocators', (locatorsError) => {
                    if(locatorsError) {
                        console.warn('Error deleting records from searchLiteLocators %s', locatorsError)
                    }
                    console.info('search-lite: Rebuilding all search indexes.');
                    this.regStack.forEach( (rS) => {
                        this.db.run('REPLACE INTO searchLiteEvents (tableId, tupleId) SELECT ' +
                                'rT.id AS tableId, sT.id AS tupleId FROM searchLiteTables rT JOIN ' + rS.tableName +
                                ' sT WHERE rT.tableName = ?', [ rS.tableName ], (error) => {
                            if(error) {
                                console.error('Scan all data error %s', error);
                            }
                        })
                    });        
                });
            });
        }
    }

    public stats(callback: any) {
        this.db.get('SELECT * FROM (' +
            '(SELECT COUNT(tagId) AS locators FROM searchLiteLocators), ' +
            '(SELECT COUNT(id) AS tags FROM searchLiteTags), ' +
            '(SELECT COUNT(id) AS tables FROM searchLiteTables), ' +
            '(SELECT COUNT(id) AS triggers FROM searchLiteTriggers), ' +
            '(SELECT COUNT(id) AS events FROM searchLiteEvents))', (error, result) => {
                if(error) {
                    console.error('stats error %s', error.message);
                    callback(null);
                } else {
                    callback(result);
                }
            }
         )
    }

    // register change event (table name, [column names], category)
    public registerEvent(tableName: string, columnNames: string[], category: string, callBack?: any) {
        if(this.createFinished) {
            this.regStack.push( { tableName: tableName, columnNames: columnNames, category: category, callback: callBack });
            let firstColumn: boolean = true;
            if(columnNames.length > 0) {
                columnNames.forEach( (c) => {
                    this.addTrigger(tableName, c, (triggerID: number) => {
                        if(firstColumn === true && category) {
                            firstColumn = false;
                            this.addCategory(category, triggerID);
                        }
                        this.processRegistrationStack();
                    });
                });
            } else {
                this.addTrigger(tableName, '', (triggerID: number) => {
                    if(category) {
                        this.addCategory(category, triggerID);
                    }
                    this.processRegistrationStack();
                });
            }
        } else {
            this.regStackPending.push( { tableName: tableName, columnNames: columnNames, category: category });
        }
    }

    private processRegistrationStack() {
        if(this.createFinished) {
            let rS = this.regStackPending.pop();
            if(rS) {
                // console.log('search-lite processRegistrationStack going to register event');
                // registerEvent does this but if rS has a callback then registerEvent wont add it
                // this.regStack.push( rS );
                this.registerEvent(rS.tableName, rS.columnNames, rS.category, rS.callback);
            }
        }
    }

    public registerWithCallback(tableName: string,
            callback: (tupleId: number) => void, columnNames: string[],
            category: string) {
        this.regStackPending.push( { tableName: tableName, columnNames: columnNames, category: category, callback: callback });
    }

    private addCategory(category: string, triggerId: number) {
        this.db.get('SELECT id FROM searchLiteCategories WHERE triggerId = ? AND category = ?',
              [ triggerId, category ], (error, result) => {
            if(error) {
                console.error('searchLite:addCategory %s', error);
            } else {
                if(!result) {
                    this.db.run('INSERT INTO searchLiteCategories (triggerId, category) VALUES (?, ?)',
                            [ triggerId, category ], (error: string) => {
                        if(error) {
                            console.error('searchLite:addCategory %s', error);
                        }
                    })
                }
            }
        });
    }

    private createTrigger(tableName: string) {
        if(!this.tablesWithTriggers.includes(tableName)) {
            this.tablesWithTriggers.push(tableName);
            this.db.run('CREATE TRIGGER IF NOT EXISTS scan_after_update_' + tableName + ' AFTER UPDATE ON ' + tableName +
                    ' BEGIN REPLACE INTO searchLiteEvents (tableId, tupleId) VALUES ' +
                    '((SELECT id FROM searchLiteTables WHERE tableName = "' + tableName + '"), new.id); END;',
                    (error) => {
                if(error) {
                    console.error('searchLite:createTrigger %s', error);
                }
            });

            this.db.run('CREATE TRIGGER IF NOT EXISTS scan_after_insert_' + tableName + ' AFTER INSERT ON ' + tableName +
                    ' BEGIN REPLACE INTO searchLiteEvents (tableId, tupleId) VALUES ' +
                    '((SELECT id FROM searchLiteTables WHERE tableName = "' + tableName + '"), new.id); END;',
                    (error) => {
                if(error) {
                    console.error('searchLite:createTrigger %s', error);
                }
            });
        };
    }

    private addTrigger(tableName: string, columnName: string, callBack: any) {
        this.db.get('SELECT rtg.id FROM searchLiteTriggers rtg JOIN searchLiteTables rtb ON rtg.tableId = rtb.id ' +
            'WHERE rtb.tableName = ? AND rtg.column = ?',
                [ tableName, columnName ], (error, value: { id: number }) => {
            if(error) {
                console.error('searchLite:addTrigger 229 %s', error);
            } else {
                if(value) {
                    callBack(value['id']);
                } else {
                    this.createTrigger(tableName);
                    this.db.run('INSERT INTO searchLiteTables (tableName) VALUES (?)', [ tableName ], () => {
                        this.db.run('REPLACE INTO searchLiteTriggers (tableId, column) VALUES ( ' +
                            '(SELECT id FROM searchLiteTables WHERE tableName = ?), ?)',
                        [ tableName, columnName ], (error) => {
                            if(error) {
                                console.error('searchLite:addTrigger 241 %s', error);
                            } else {
                                this.db.get('SELECT rtg.id FROM searchLiteTriggers rtg JOIN searchLiteTables rtb ON rtg.tableId = rtb.id ' +
                                    'WHERE rtb.tableName = ? AND rtg.column = ?',
                                [ tableName, columnName ], (error, value: { id: number }) => {
                                    if(error) {
                                        console.error('searchLite:addTrigger 247 %s', error);
                                    } else {
                                        callBack(value['id']);
                                    }
                                });
                            }
                        })

                    })
                }
            }
        });

    }

    // Could return a set of searchLiteLocators or an array of tableNames and tuples.
    public search(searchText: string[], callback: any, category: string) {
        console.time('Searching for "'+searchText+'"');
        let searchMarkers = '';
        let searchArray = new Array<string>();
        searchText.forEach( (s) => {
            if(!searchMarkers) {
                searchMarkers += '?';
            } else {
                searchMarkers += ', ?';
            }
            searchArray.push(s.toLowerCase());
        });
        searchArray.push(category);

        this.db.all('SELECT DISTINCT (rL.tupleId), rT.tableName FROM searchLiteLocators rL ' +
            'JOIN searchLiteTables rT ON rL.tableId = rT.id ' +
            'JOIN searchLiteTags rI ON rI.id = rL.tagId AND rI.id IN ' +
            '(SELECT id FROM searchLiteTags WHERE tag IN (' + searchMarkers + ')) ' +
            'JOIN searchLiteCategories rC ON rC.id = rL.categoryId AND rC.category = ?',
            searchArray, (error, result) => {
                if(error) {
                    console.error('search: %s', error);
                } else {
                    console.timeEnd('Searching for "'+searchText+'"');
                    callback(result);
                }
            })
    }
     
    // Could return a set of searchLiteLocators or an array of tableNames and tuples.
    public extendedSearch(searchText: string[], callback: any, categories: string[]) {
        console.time('Searching for "'+searchText+'"');
        let searchMarkers = '';
        let searchArray = new Array<string>();
        let categoryMarkers = '';
        searchText.forEach( (s) => {
            if(!searchMarkers) {
                searchMarkers += '?';
            } else {
                searchMarkers += ', ?';
            }
            searchArray.push(s.toLowerCase());
        });
        if(categories) {
            categories.forEach( (c: string) => {
                if(categoryMarkers) {
                    categoryMarkers = categoryMarkers + ', ';
                }
    
                categoryMarkers = categoryMarkers + '?';
                searchArray.push(c);
            });
        }

        this.db.all('SELECT rL.tupleId, rT.tableName, rC.category FROM searchLiteLocators rL ' +
            'JOIN searchLiteTables rT ON rL.tableId = rT.id ' +
            'JOIN searchLiteTags rI ON rI.id = rL.tagId AND rI.id IN ' +
            '(SELECT id FROM searchLiteTags WHERE tag IN (' + searchMarkers + ')) ' +
            'JOIN searchLiteCategories rC ON rC.id = rL.categoryId AND rC.category IN (' + categoryMarkers + ')',
            searchArray, (error, result: {tupleId: number, tableName: string, category: string}[]) => {
                if(error) {
                    console.error('search: %s', error);
                } else {
                    if(result[0]) {
                        let theTableName = result[0]['tableName'];

                        // The SQL command DISTINCT is not able to remove rows that include category.
                        // After the WITH set is selected and sorted the duplicates can be removed
                        let rebuildArray = new Array<{ id: number, category: string, sort: number}>;
                        result.forEach( (row) => {
                            let sort = categories.indexOf(row.category);
                            let rD = rebuildArray?.find( (r) => r.id === row.tupleId);
                            if(!rD) {
                                rebuildArray.push({ id: row.tupleId, category: row.category, sort: sort});
                            } else {
                                if(sort < rD.sort) {
                                    rD.sort = sort;
                                    rD.category = row.category;
                                }
                            }
                        });
                        
                        let theWithSet = 'WITH os(sort, category, id) AS (VALUES ';
                        let allTupleIds = '(';
                        rebuildArray.forEach( (r) => {
                            theWithSet += '(' + r.sort + ', "' + r.category + '", ' + r.id + '), ';
                            allTupleIds += r.id + ', ';
                        });
                        allTupleIds = allTupleIds.substring(0, allTupleIds.length - 2);
                        allTupleIds += ')';
                        theWithSet = theWithSet.substring(0, theWithSet.length - 2);
                        theWithSet += ') ';
                        let SqlQuery = theWithSet + 'SELECT tn.*, os.sort, os.category FROM ' + theTableName + ' tn JOIN os ON os.id = tn.id ' +
                            'WHERE tn.id IN ' + allTupleIds + ' ORDER BY os.sort';
                        this.db.all(SqlQuery, (error, result ) => {
                            if(error) {
                                console.error('Selecting with category values %s', error.message);
                                console.info(SqlQuery);
                            } else {
                                console.timeEnd('Searching for "'+searchText+'"');
                                callback(result);
                            }
                        })
                    } else {
                        callback({});
                    }
                }
            })
    }

}